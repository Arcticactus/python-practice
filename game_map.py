# Cr: please don't use single quotes, it's far better to use double quotes everywhere rather than mix up double and
# single quotes since single quotes are usually used to define single chars, but that rule brakes many times.
# Cr: Rethink about the way the map should be built, you have an algorithm as for how the map should be drawn, but the
# fact that you need to calculate what's in there from the snake and the food complicates it, how could you make the map
# know what it has in every point ?

import os
import sys

# Cr: Do you really need to define all of those three CONSTS ?
MAP_SIZE = (15, 15)
MAP_HEIGHT = MAP_SIZE[0]
MAP_LENGTH = MAP_SIZE[1]
########################
ROW_INDEX = 0
COLUMN_INDEX = 1
FOOD_SHAPE = ' * '
SNAKE_SHAPE = ':0:'
SNAKE_HEAD = ':@:'
BORDER_SHAPE = 'X'
UPPER_LOWER_BORDER = (BORDER_SHAPE + ' ') * 24  # Fits the map.


def display_map(snake, food):
    """
    Displays the current game map.
    Updates the location of the food and the position of the snake.
    """
    os.system('cls')
    #  The map is not a constant because than deepcopy would be needed.
    # Cr: For what ?! I'm sorry but ' _ ' is the exact difference of what variables should be named, even if the
    # variable is declared and used in one line, it needs to be descriptive of what it  represents.
    current_map = [['   ' for _ in range(MAP_LENGTH)] for _ in range(MAP_HEIGHT)]
    food_row, food_column = food.get_food_coordinates()
    current_map[food_row][food_column] = FOOD_SHAPE

    head_row, head_column = snake.get_snake_head()
    current_map[head_row][head_column] = SNAKE_HEAD

    for part in snake.body[1:]:
        if not touched_wall(part[ROW_INDEX], part[COLUMN_INDEX]):
            current_map[part[ROW_INDEX]][part[COLUMN_INDEX]] = SNAKE_SHAPE

    sys.stdout.write(UPPER_LOWER_BORDER + '\n')
    for row in current_map:
        sys.stdout.write(BORDER_SHAPE)
        for column in row:
            sys.stdout.write(column)
        sys.stdout.write(BORDER_SHAPE + '\n')

    sys.stdout.write(UPPER_LOWER_BORDER + '\n')
    sys.stdout.write("\n\t\tCurrent Score: " + str(food.score))
    sys.stdout.flush()


def touched_wall(x, y):
    """
    Returns false if the coordinates don't touch the map borders.
    Raises an exceptions otherwise.
    """
    # Cr: do not assign a lambda expression, use a def, but you'll rework the way this works I HOPE so that's just for
    # future proofing =]
    not_in_map = lambda i: not 0 <= i <= 14
    if not_in_map(x) or not_in_map(y):
        raise IndexError
    return False
