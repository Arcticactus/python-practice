# Cr: please do rethink this entire module and the best way to implement it, think what the snake is made of, and what
# is the best way, since you have the head's location , and the direction the snake is going towards you shouldn't
# iterate over it's entire body to move every specific part to it's next location, most of the body "stays" in place.

from game_map import display_map

W_KEY = chr(119).encode()
A_KEY = chr(97).encode()
S_KEY = chr(115).encode()
D_KEY = chr(100).encode()
keys = (W_KEY, A_KEY, S_KEY, D_KEY)


class Snake:
    def __init__(self):
        self.head = [7, 7]  # Initial snake position on the map.
        self.tail = self.head[:]
        self.body = [self.head]

    def move_snake(self, key, food):
        """
        Moves the snake in the direction determined by the key passed.
        The screen would then be updated.
        """
        # Cr: that's fine, I would rather to have the snake not move and wait for the first key, and if you have these
        # kinds of statements make them one liners, this is python after all =]
        # I thought that this would look better than None as a dictionary key.
        if key is None:
            key = D_KEY  # The default direction of the snake.

        new_head = {W_KEY: [self.head[0] - 1, self.head[1]],
                    A_KEY: [self.head[0], self.head[1] - 1],
                    S_KEY: [self.head[0] + 1, self.head[1]],
                    D_KEY: [self.head[0], self.head[1] + 1]}[key]

        self.move_body(new_head, food)
        display_map(self, food)

    def move_body(self, new_head, food):
        """
        Moves the snake's body forward.
        If the snake collides with itself, an exception is raised.
        """
        is_perpendicular = True
        is_horizontal = True
        for i in range(len(self.body) - 1, 0, -1):
            if self.body[i][0] != self.body[i - 1][0]:
                is_horizontal = False
            if self.body[i][1] != self.body[i - 1][1]:
                is_perpendicular = False
            self.body[i] = self.body[i - 1][:]

        # Snake collision
        if new_head in self.body and not (is_perpendicular or is_horizontal):
            raise IndexError

        food_row, food_col = food.get_food_coordinates()
        if new_head == [food_row, food_col]:
            food.appears = False
            food.score += 1
            self.body.append(self.tail)  # The snake grows.

        self.body[0] = self.head = new_head
        self.tail = self.body[-1][:]

    def get_snake_head(self):
        return self.head[0], self.head[1]
