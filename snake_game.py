# Cr: please merge this module with single_game and banner, it's kinda useless to have three modules that are so short,
#   and there's no need to make a single game module, it could easily be a function here

import os
from single_game import play_one_game
from banner import GAME_BANNER, LOST_TEXT

PLAY_COMMANDS = ('Y', 'y')
PLAY_MESSAGE = "Want to play? Press Y or y "
# Cr: please, color a is for cyber =]
GREEN_COLOR_CMD = 'color 2'


def main():
    os.system(GREEN_COLOR_CMD)
    print GAME_BANNER
    highest_score = 0
    play = raw_input(PLAY_MESSAGE)
    while play in PLAY_COMMANDS:
        result = play_one_game()
        print LOST_TEXT
        highest_score = max(highest_score, result)
        print "Highest Score: ", highest_score
        play = raw_input(PLAY_MESSAGE)


if __name__ == "__main__":
    main()
