# Cr: merge with snake_game and banner

import msvcrt
from time import sleep
from snake import Snake, keys
from game_map import display_map
from food import Food


def play_one_game():
    """
    Plays the snake game until the player lost.
    Loss conditions:
    1. The snake hits the walls.
    2. The snake hits itself.
    :return: the score of this round and clears the screen afterwards.
    """
    food = Food()
    snake = Snake()
    display_map(snake, food)
    try:
        # Cr: I see no reason for this line to be under the try statement, try to keep variable declarations as early as
        #  possible and relative
        previous_key = None
        while True:
            while not msvcrt.kbhit():
                sleep(0.13)  # To prevent the snake from going too fast.
                snake.move_snake(previous_key, food)
            key = msvcrt.getch()
            # Cr: We would like to do such things if our indented block will indent another block of code, since this is
            #  not the case, there is no need to have the if include only relevant keys
            if key not in keys or key == previous_key:  # Ignores irrelevant keys.
                continue
            snake.move_snake(key, food)
            previous_key = key
    except IndexError:
        return food.score
    finally:
        from os import system
        system('cls')
