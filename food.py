# Cr: do you really need an entire class for the food, you always have the same food, only it's location changes.
# It would be nice if the game had different kinds of foods, we have classes when we want to define objects, you're not
# wrong but if it's not necessary, try to avoid it.

from random import randint

GAME_MAP_BOUNDARIES = (0, 14)
UPPER_LEFT_LIMIT = GAME_MAP_BOUNDARIES[0]
LOWER_RIGHT_LIMIT = GAME_MAP_BOUNDARIES[1]


class Food:
    def __init__(self):
        self.score = 0
        self.appears = True
        self.row = randint(UPPER_LEFT_LIMIT, LOWER_RIGHT_LIMIT)
        self.column = randint(UPPER_LEFT_LIMIT, LOWER_RIGHT_LIMIT)

    def get_food_coordinates(self):
        """
        :return: The coordinates of the food on the screen.
        :type: tuple, 0 index is row, 1 index is column.
        If the food doesn't appear on the screen, new coordinates are randomized.
        """
        if not self.appears:
            self.row = randint(UPPER_LEFT_LIMIT, LOWER_RIGHT_LIMIT)
            self.column = randint(UPPER_LEFT_LIMIT, LOWER_RIGHT_LIMIT)
            self.appears = True
        return self.row, self.column
